<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        \DB::table('users')->insert(
            array(
              array('id' => '1',
                  'first_name' => 'Erika Joyce',
                  'middle_name' => '',
                  'last_name' => 'Agliam',
                  'complete_address' => 'Mabini Santiago City Isabela',
                  'contact_number' => '',
                  'username' => 'admin',
                  'email_address' => 'admin@gmail.com',
                  'email_verified_at' => date("Y-m-d H:i:s"),
                  'password'=>'$2y$10$1PjQix6XGR5WpOtsIhzzMu.TEUSxv/142J4vlIJL.oQruUD/ybhu.',
                  'type'=> 1,
                  'status' => 1,
                  'created_at' => date("Y-m-d H:i:s"))
            )
        );
    }
}
