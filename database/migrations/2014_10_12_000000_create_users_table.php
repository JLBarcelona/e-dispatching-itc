<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('suffix')->nullable();            
            $table->string('complete_address');
            $table->string('email_address')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('username')->nullable();
            
            // vehicle
            $table->string('color')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->string('plate_number')->nullable();
            $table->string('vehicle_type')->nullable();
            $table->string('capacity')->nullable();
            // vehicle

            // driver
            $table->string('driver_license_no')->nullable();
            $table->string('driver_license_expiration')->nullable();
            $table->tinyInteger('have_conductor')->nullable();
            $table->string('conductor_name')->nullable();
            // driver
            
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('type')->unsigned()->default(2);
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->string('forgot_password_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
