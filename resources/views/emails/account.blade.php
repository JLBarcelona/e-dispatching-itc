<p>Hello {{ $user->fullname }},</p>

<p>Your account details are as follows:</p>

<ul>
    <li><strong>Type:</strong> {{ $user->getTypeText($user->type) }}</li>
    <li><strong>username:</strong> {{ $user->username }}</li>
    <li><strong>Password:</strong> {{ $password }}</li>
    <li><strong>Login URL:</strong> {{ url('') }}</li>
</ul>

<p>Thank you for registering with us!</p>

<p>Regards,<br>{{ config('app.name') }}</p>