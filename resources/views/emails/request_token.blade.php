<p>Hello {{ $data['name'] }},</p>

<p>Reset Password</p>

<ul>
    <li><strong>Reset Password URL:</strong> {{ $data['url'] }}</li>
</ul>

<p>If you don't request for password reset, just ignore this.</p>

<p>Regards,<br>{{ config('app.name') }}</p>