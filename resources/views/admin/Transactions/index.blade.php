@extends('admin.layout.app')
@section('title', 'Monitoring')

@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <div class="container-fluid">
      <div class="card d-none">
        <div class="card-header" onclick="$('#minimize_filter').click()" >
          <h5 class="card-title">Record Filter</h5>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
            
            </button>
          </div>
        </div>
        
        <div class="card-body">
          <div class="row">
            <div class="position-relative mb-2 col-md-4">
              <label>Fullname or Address</label>
              <input type="text" name="filter" id="filter" class="form-control" placeholder="Search here">
              
            </div>
          </div>
          
        </div>
        <div class="card-footer text-right">
          <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
          <i class="fa fa-minus"></i> Minimize Filter
          </button>
          <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
          <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
        </div>
      </div>

      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
          <table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;">
            <thead>
              <tr>
                <th class="text-center" rowspan="2">Route</th>
                <th class="text-center" rowspan="2">Driver</th>
                <th class="text-center" rowspan="2">Vehicle</th>
                <th class="text-center" rowspan="2">Male Passengers</th>
                <th class="text-center" rowspan="2">Female Passengers</th>
                <th class="text-center" rowspan="2">Total Passengers</th>
                <th class="text-center" colspan="3">Dispatching</th>
                <th class="text-center" rowspan="2">Options</th>
              </tr>
              <tr>
                <th class="text-center"><small><b>Departure</b></small></th>
                <th class="text-center"><small><b>Arrival</b></small></th>
                <th class="text-center"><small><b>Status</b></small></th>

              </tr>
            </thead>

          </table>
        </div>
      </div>
    </div>
  </div>


  <!-- modal here -->
    <form class="needs-validation" id="vehicle_form" action="{{ route('admin.transaction.save') }}" novalidate>
      <div class="modal fade" tabindex="-1" role="dialog" id="modal_vehicle_form" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal_vehicle_form_title">Add Transaction</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                @csrf
                <input type="hidden" name="id" id="id" />
                <div class="col-md-12 mb-2">
                  <label>Route</label>
                  <input type="text" name="route" id="route" class="form-control " placeholder="Enter route here" required/>
                </div>
                <div class="col-md-12 mb-2">
                  <label>Driver</label>
                  <select id="driver" name="driver" class="form-control" required>
                    <option value="">Select Driver</option>
                    @foreach(getUserByType(4) as $users)
                      <option value="{{ $users->id }}" data-capacity="{{ $users->capacity }}">{{ $users->last_name.' '.$users->first_name.' '.$users->suffix }}</option>
                    @endforeach
                  </select>
                  <input type="hidden" id="max_cap">
                </div>
              </div>
              <div class="row passenger_details d-none">
                  <div class="col-sm-12 mt-3"></div>
                  <div class="col-sm-4"><hr></div>
                  <div class="col-sm-4 text-center"><h4>Passengers</h4></div>
                  <div class="col-sm-4"><hr></div>
                  <div class="col-md-12">
                    <h5 class="text-muted">Capacity: <b><span id="cap"></span></b></h5>
                  </div>
                  <div class="col-md-6 mb-2">
                    <label>Number of Male</label>
                    <input type="number" name="male" id="male" class="form-control" oninput="checkMaxCap(this);" placeholder="Enter Number of Male here" required/>
                  </div>
                  <div class="col-md-6 mb-2">
                    <label>Number of Female</label>
                    <input type="number" name="female" id="female" class="form-control " oninput="checkMaxCap(this);" placeholder="Enter Number of Female here" required/>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-success" id="vehicle_form_btn" >Save</button>
            </div>
          </div>
        </div>
      </div>
    </form>

   <div class="modal" tabindex="-1" role="dialog" id="user_details" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">User Account</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Please take a screenshot or copy the user account.</p>
            <p>Username: <span id="username_account"></span></p>
            <p>Password: <span id="password_account"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success">Ok</button>
          </div>
        </div>
      </div>
    </div>
  <!-- modal here -->
@endsection

@push('page-modals')

@endpush

@push('js-scripts')
<script type="text/javascript">
  $("#driver").on('input', function(e){
    if ($(this).val().trim() == '') {
      $(".passenger_details").addClass('d-none');
      $("#male").val('');
      $("#female").val('');
      $("#max_cap").val('');
    }else{
      $(".passenger_details").removeClass('d-none');
      var selectedOption = $("#driver option:selected");
      var dataCustomValue = selectedOption.data("capacity");
      $("#max_cap").val(dataCustomValue);
      $("#cap").text(dataCustomValue);
    }
  });

  function checkMaxCap(_this){
    let male = $("#male").val();
    let female = $("#female").val();
    let max = Number($("#max_cap").val());

    let total = (Number(male) + Number(female));
    if (total > max) {
      $(_this).val("");
      $("#cap").removeClass('text-success');
      $("#cap").addClass('text-danger');
    }else{
      $("#cap").removeClass('text-danger');
      $("#cap").addClass('text-success');
    }
  }

  $("#vehicle_form").on('submit', function(e){
    e.preventDefault();
    let id = $('#id').val();
    let url = $(this).attr('action');
    let formData = $(this).serialize();
    $.ajax({
      type:"POST",
      url:url+'/'+id,
      data:formData,
      dataType:'json',
      beforeSend:function(){
        $('#vehicle_form_btn').prop('disabled', true);
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          $('#modal_vehicle_form').modal('hide');
           Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          });
        show_user_accounts();
        }else{
          console.log(response);
        }
          validation('vehicle_form', response.error);
          $('#vehicle_form_btn').prop('disabled', false);
      },
      error: function(error){
        $('#vehicle_form_btn').prop('disabled', false);
        console.log(error);
      }
    });
  });

  function add_vehicles(){
    $("#id").val('');
    $("#route").val('');
    $("#driver").val('');
    $("#male").val('');
    $("#female").val('');
    $('.modal-title').html('Add Transaction');
    $("#modal_vehicle_form").modal('show');
  }


  function edit_vehicles(id){
    $.ajax({
      type:"GET",
      url:"{{ route('admin.transaction.find') }}/"+id,
      data:{},
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
        console.log(response);
        if (response.status == true) {
          $("#id").val(response.data.id);
          $("#route").val(response.data.routes);
          $("#driver").val(response.data.driver_id);
          $("#male").val(response.data.male);
          $("#female").val(response.data.female);
          $('.modal-title').html('Edit Transaction');
          $('#modal_vehicle_form').modal('show');
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
    });
  }

  $('#refresh-filter').on('click', function(){
    $('#filter').val('');
    submit_filter();
  });

  function submit_filter(){
    show_user_accounts();
    
  }




  show_user_accounts();
  var tbl_user_accounts;
  function show_user_accounts(){
    if (tbl_user_accounts) {
      tbl_user_accounts.destroy();
    }
      tbl_user_accounts = $('#tbl_user_accounts').DataTable({
    
      @if(auth()->user()->type !== 4)
       dom: 'Bfrtip',
       dom: "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
             buttons: [
              @if(Auth::user()->type !== 1 && Auth::user()->type !== 3)
              {
                  text: 'Add Transaction',
                  className: "btn btn-info",
                  action: function ( e, dt, node, config ) {
                      $('#modal_vehicle_form').modal('show');
                      add_vehicles();
                  }
              }
              @endif
          ],
      @endif
      language: {
      "emptyTable": "No data available"
      },
    ajax: {
          url: "{{ route('admin.transaction.list') }}",
          data: {filter : $('#filter').val()},
      },
      columns: [{
      className: '',
      "data": "routes",
      "title": "Route",
    },{
      className: '',
      "data": "driver.fullname",
      "title": "Driver",
    },{
      className: '',
      "data": "vehicle.plate_number",
      "title": "Vehicle",
      "render":function(data, type, row, meta){
        return '('+row.driver.plate_number+') '+row.driver.vehicle_model+'-'+row.driver.vehicle_type; 
      }
    },{
      className: '',
      "data": "male",
      "title": "Male Passengers",
    },{
      className: '',
      "data": "female",
      "title": "Female Passengers",
    },{
      className: '',
      "data": "id",
      "title": "Total Passengers",
       "render":function(data, type, row, meta){
        return (row.male+row.female);
      }
    },{
      className: '',
      "data": "departure_time",
      "title": "Departure Time",
      "render":function(data){
        if (data !== '') {
          return data;
        }else{
          return 'N/A';
        }
      }
    },{
      className: '',
      "data": "arrival_time",
      "title": "Arrival",
      "render":function(data){
        if (data !== '') {
          return data;
        }else{
          return 'N/A';
        }
      }
    },{
      className: 'text-center',
      "data": "status_text",
      "title": "Status",
      "render":function(data, type, row, meta){
        return  '<span class="badge '+row.status_color+'">'+data+'</span>';
      }
    },

      @if(auth()->user()->type !== 4)
        {
          className: 'text-center',
          "data": "id",
          "title": "Options",
          "orderable": false,
          "render": function(data, type, row, meta){

            if (row.status == 1) {
              newdata = '<button type="button" class="btn btn-sm btn-info mb-2" disabled><i class="fa fa-edit"></i></button>\
                         <button type="button" class="btn btn-sm btn-danger mb-2" disabled><i class="fa fa-trash"></i></button>'
            }else{
              newdata = '<button type="button" class="btn btn-sm btn-info mb-2" onclick="edit_vehicles('+data+')"><i class="fa fa-edit"></i></button>\
                         <button type="button" class="btn btn-sm btn-danger mb-2" onclick="delete_vehicle('+data+')"><i class="fa fa-trash"></i></button>'
            }

            return newdata;
          }
        }
      @endif
    ]
    });
  }




  function delete_vehicle(id){

    Swal.fire({
        title: 'Are you sure?',
        text: "This user will be deleted!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
        type:"delete",
        url:"{{ route('admin.transaction.delete') }}/"+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          show_user_accounts();
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

          Swal.fire(
            'Deleted!',
            'User has been deleted.',
            'success'
          )
        }
      })

    
  }

  
</script>
@endpush
