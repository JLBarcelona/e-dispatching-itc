@extends('admin.layout.app')
@section('title', 'Profile Settings')

@push('styles')

<style>
    
</style>

@endpush

@section('content')


  <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <form id="account_form" action="{{ route('admin.profile_settings.store') }}" method="POST">
                            <div class="card card-info card-outline">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12 mb-2">
                                                    <label>firstname</label>
                                                    <input class="form-control" type="text" id="first_name" name="first_name" value="{{ $user->first_name }}">
                                                </div>
                                                <div class="col-sm-12 mb-2">
                                                    <label>Middlename</label>
                                                    <input class="form-control" type="text" id="middle_name" name="middle_name" value="{{ $user->middle_name }}">
                                                </div>
                                                <div class="col-sm-12 mb-3">
                                                    <label>Lastname</label>
                                                    <input class="form-control" type="text" id="last_name" name="last_name" value="{{ $user->last_name }}">
                                                </div>
                                                <div class="col-sm-12 mb-3">
                                                    <label>Email Address</label>
                                                    <input class="form-control" type="text" id="email_address" name="email_address" value="{{ $user->email_address }}">
                                                </div>
                                                <div class="col-sm-12 mb-3">
                                                    <label>Username</label>
                                                    <input class="form-control" type="text" id="username" name="username" value="{{ $user->username }}">
                                                </div>
                                                <div class="col-sm-12 mb-2">
                                                    <button id="btn_change_pass" type="button" class="btn btn-default" onclick=""><i class="fas fa-key"></i>&nbsp;Change Password</button>

                                                    <button type="submit" class="btn btn-info d-none" id="account_form_btn" >Submit</button>
                                                    <button type="button" class="btn btn-primary float-right" onclick="onsubmitSetting();">Submit</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form id="uploadForm" enctype="multipart/form-data">
                            @csrf
                            <div class="card card-info card-outline">
                                <div class="card-body text-center" style="height:501px;">
                                    <img src="{{ Auth::user()->getAvatarImg() }}" class="img-circle elevation-2" style="max-width:30%;" id="preview">
                                    <br><br><br>
                                    <input type="file" name="file" id="fileInput" class="d-none" />
                                    <div class="mt-1">
                                        <button type="button" class="btn btn-primary" onclick="$('#fileInput').click()">Browse Image</button>
                                        <button type="button" class="btn btn-success" onclick="uploadFile()">Apply Changes</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
    </section>


 
  
  <!-- modal here -->
@endsection

@push('page-modals')
<div class="modal fade" id="modal_password" role="dialog">
        <div class="modal-dialog modal-md">

            <form id="frm_password" action="{{ route('admin.profile_settings.change_password') }}" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <label>Old Password</label>
                                <div class="invalid-feedback text-left" id="err_middle_name"></div>
                                <div class="input-group">
                                  <input type="password" class="form-control" name="password" id="password">
                                  <div class="input-group-append">
                                    <div class="input-group-text">
                                      <span class="fas fa-eye" onclick="password_toggler(this, '#password');"></span>
                                    </div>
                                  </div>
                            </div>

                            </div>

                            <div class="col-md-12 mb-4">
                                <label>New Password</label>
                                <div class="input-group">
                                  <input type="password" id="new_password" name="new_password" class="form-control">
                                  <div class="input-group-append">
                                    <div class="input-group-text">
                                      <span class="fas fa-eye" onclick="password_toggler(this, '#new_password');"></span>
                                    </div>
                                  </div>
                            </div>
                            </div>


                            <div class="col-md-12 mb-4">
                                <label>Confirm Password</label>
                                <div class="input-group">
                                  <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                                  <div class="input-group-append">
                                    <div class="input-group-text">
                                      <span class="fas fa-eye" onclick="password_toggler(this, '#confirm_password');"></span>
                                    </div>
                                  </div>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="password_form_btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>


        </div>
    </div>

@endpush

@push('js-scripts')
<script type="text/javascript">
      $('#fileInput').change(function() {
            previewImage();
        });

        function previewImage() {
            var fileInput = $('#fileInput')[0];
            var preview = $('#preview');

            // Display preview when a valid image is selected
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    preview.attr('src', e.target.result);
                    preview.show();
                };

                reader.readAsDataURL(fileInput.files[0]);
            } else {
                // Hide preview if no file is selected
                preview.hide();
            }
        }

     function password_toggler(_this, selector){
      $(_this).toggleClass('fa-eye fa-eye-slash')
      $(selector).attr('type', function(index, attr){
          return attr == 'text' ? 'password' : 'text';
      });
    }

     function uploadFile() {
        var formData = new FormData($('#uploadForm')[0]);

        $.ajax({
            url: '{{ route("admin.user_management.upload.file") }}',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.status === true) {
                     Swal.fire(
                        'Uploaded!',
                        'User avatar saved successfully.',
                        'success'
                      );
                }
            },
            error: function(xhr) {
                // Handle errors
                var errors = xhr.responseJSON.errors;
                var errorMessage = '<div class="alert alert-danger"><ul>';
                $.each(errors, function (key, value) {
                    errorMessage += '<li>' + value[0] + '</li>';
                });
                errorMessage += '</ul></div>';
                $('#result').html(errorMessage);
            }
        });
    }

  function onsubmitSetting(){
    let first_name = $("#first_name").val();
    let last_name = $("#last_name").val();
    let email_address = $("#email_address").val();
    let username = $("#username").val();
    if (first_name == '' || last_name == '' || email_address == '' || username == '') {
        $("#account_form_btn").click();
    }else{
        Swal.fire({
        title: 'Are you sure!',
        text: "Do you want to apply changes?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
            $("#account_form_btn").click();
        }
      })
    }
    
   
  }

  $("#account_form").on('submit', function(e){
    var mydata = $(this).serialize();
    let url = $(this).attr('action');
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
      type:"post",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#account_form_btn').prop('disabled', true);
          $('#account_form_btn').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                  })
          
        }else{
          
          //<!-- your error message or action here! -->
          validation('account_form', response.errors);

        }
        $('#account_form_btn').prop('disabled', false);
        $('#account_form_btn').text('Save');
      },
      error:function(error){
        console.log(error)
      }
    });
  });






$('#btn_change_pass').click(function(){
            $('#frm_password')[0].reset();
            $('#modal_password .modal-title').text('Change Password');
            $('#modal_password').modal('show');
        });



  $("#frm_password").on('submit', function(e){
    var mydata = $(this).serialize();
    let url = $(this).attr('action');
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
      type:"post",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_submit').prop('disabled', true);
          $('#btn_submit').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                  })
          
          $('#modal_password').modal('hide');
        }else{
          if (response.message) {
            Swal.fire({
                    position: 'center',
                    icon: 'info',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                  })
          }
          //<!-- your error message or action here! -->
          custom_err.display_all(response.errors,$('#frm_password'));

        }
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Save');
      },
      error:function(error){
        console.log(error)
      }
    });
  });


  

  function edit_user(id){
    $.ajax({
      headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
            type:"get",
            url:"{{ route('admin.user_management.find') }}/"+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
              $('#hide_on_edit').hide();
                $('#first_name').val(response.data.first_name);
                $('#middle_name').val(response.data.middle_name);
                $('#last_name').val(response.data.last_name);
                $('#gender').val(response.data.gender);
                $('#contact').val(response.data.contact);
                $('#address').val(response.data.address);
                $('#bday').val(response.data.bday);
                $('#bday').val(response.data.height);
                $('#bday').val(response.data.weight);
                $('#id').val(response.data.id);
                $('.modal-title').html('Edit User');
                $('#modal_add_edit').modal('show');
                
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
  }


  
</script>
@endpush
