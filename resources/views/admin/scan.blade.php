@extends('admin.layout.app')
@section('title', 'Scanner')

@push('styles')
<style type="text/css">
  #reader{
    width: auto !important; position: relative; padding: 0px; border: 1px solid silver;
  }
  button{
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }
  button {
  color: #fff;
  background-color: #007bff;
  border-color: #007bff;
  box-shadow: none;
}

  button:hover {
  color: #fff;
  background-color: #0069d9;
  border-color: #0062cc;
}

  button:focus{
  color: #fff;
  background-color: #0069d9;
  border-color: #0062cc;
  box-shadow: 0 0 0 0 rgba(38, 143, 255, 0.5);
}
</style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      	<div class="row">
      		<div class="col-lg-12">
              <div style="width:500px;" id="reader"></div>
            </div>
      	</div>
      </div>
    </section>
    <!-- /.content -->

@endsection

@push('page-modals')

@endpush

@push('js-scripts')
  

<script type="text/javascript">

  function onScanSuccess(qrCodeMessage) {
    // alert(qrCodeMessage);
    
    $.ajax({
        type:"get",
        url:'{{ route('admin.scanning') }}',
        data:{id:qrCodeMessage},
        dataType:'json',
        beforeSend:function(){
          $("button").click();
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            // swal("Success", response.message, "success");
             Swal.fire({
              position: 'center',
              icon: 'success',
              title: response.message,
              showConfirmButton: false,
              timerProgressBar: true,
              timer: 5000
            }).then(() => {
              location.reload();
            });
         }else if (response.status == false) {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: response.message,
              showConfirmButton: false,
              timerProgressBar: true,
              timer: 5000
            }).then(() => {
              location.reload();
            });

         }else{
          console.log('failed here');
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
      


  }
  function onScanError(errorMessage) {
    //handle scan error
    console.log(errorMessage);
  }
  var html5QrcodeScanner = new Html5QrcodeScanner(
      "reader", { fps: 10, qrbox: 250 });
  html5QrcodeScanner.render(onScanSuccess, onScanError);
</script>
@endpush
