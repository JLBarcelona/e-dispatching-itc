@extends('admin.layout.app')
@section('title', 'Dashboard')

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        @if(auth()->user()->type !== 4)
          <div class="row">
            <div class="col-lg-7">
              <div class="row">
                <!-- ./col -->
                <div class="col-lg-12 col-12">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>{{ getTransaction() }}</h3>
                      <p>Today's Transactions</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-list nav-icon"></i>
                    </div>
                  </div>
                </div>

               
                 <!-- ./col -->
                <div class="col-lg-4 col-6">
                  <!-- small box -->
                  <div class="small-box bg-primary">
                    <div class="inner">
                      <h3>{{ getUsersCount(2); }}</h3>
                      <p>Dispatcher Accounts</p>
                    </div>
                    <div class="icon">
                      <!-- <i class="fas fa-bus nav-icon"></i> -->
                      <i class="fas fa-people-arrows nav-icon"></i>
                    </div>
                  </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-4 col-6">
                  <!-- small box -->
                  <div class="small-box bg-danger">
                    <div class="inner">
                      <h3>{{ getUsersCount(3); }}</h3>
                      <p>Monitor Accounts</p>
                    </div>
                    <div class="icon">
                      <!-- <i class="fas fa-bus nav-icon"></i> -->
                      <i class="fas fa-user-shield nav-icon"></i>
                    </div>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-6">
                  <!-- small box -->
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>{{ getUsersCount(4); }}</h3>
                      <p>Driver Accounts</p>
                    </div>
                    <div class="icon">
                      <!-- <i class="fas fa-bus nav-icon"></i> -->
                      <i class="fas fa-id-card nav-icon"></i>
                    </div>
                  </div>
                </div>
                 <div class="col-lg-4 col-6">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>{{ getVehicleCount(2); }}</h3>
                      <p>Bus</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-bus-alt nav-icon"></i>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-6">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>{{ getVehicleCount(1); }}</h3>
                      <p>Van</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-shuttle-van nav-icon"></i>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-6">
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>{{ getVehicleCount(3); }}</h3>
                      <p>Mini Bus/Modernize Jeepney</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-bus nav-icon"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5">
                <div class="card">
                  <div class="card-header">Today's Passengers</div>
                  <div class="card-body" style="height:327px;">
                    @if(empty(getTransactionMaleFemale('male')) && empty(getTransactionMaleFemale('female')))
                    <h3 class="text-muted text-center">No Passengers found.</h3>
                    @else
                      <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    @endif

                  </div>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-header">Today's Transactions <b>({{ date('Y-m-d') }})</b>
                  <a target="blank" class="btn btn-primary float-right" href="{{ route('admin.transaction.pdf') }}"><i class="fa fa-print"></i> Print Report</a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered table-stripped">
                        <thead>
                            <tr>
                                <th colspan="2">Date: {{ date('Y-m-d') }}</th>
                                <th colspan="7" class="text-center">DISPATCHER REPORT</th>
                                <th colspan="1">SHIFT:</th>
                            </tr>
                            <tr>
                                <th>No.</th>
                                <th>Name of Driver</th>
                                <th>Plate No.</th>
                                <th>Route</th>
                                <th>No. of Passengers</th>
                                <th>Time of Arrival</th>
                                <th>Time of Departure</th>
                                <th>Male Passengers</th>
                                <th>Female Passengers</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1; @endphp
                            @foreach(getTodays() as $dt)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $dt->driver->fullname }}</td>
                                    <td>{{ $dt->driver->plate_number }}</td>
                                    <td>{{ $dt->routes }}</td>
                                    <td>{{ (intval($dt->male) + intval($dt->female))  }}</td>
                                    <td>{{ $dt->arrival_time }}</td>
                                    <td>{{ $dt->departure_time }}</td>
                                    <td>{{ $dt->male }}</td>
                                    <td>{{ $dt->female }}</td>
                                    <td>{{ $dt->status_text }}</td>
                                </tr>
                                @php $i++ @endphp
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @elseif(auth()->user()->type === 4)
        <div class="row" id="transactions_code">
          @forelse(getMyRoute() as $routes)
            <div class="col-lg-4 col-12">
              <div class="small-box {{ $routes->status_color }}">
                <div class="inner">
                  <p class="m-0">Status: {{ $routes->status_text }}</p>
                  <h3>{{ $routes->routes }}</h3>
                  <p class="m-0">Male: <b>{{ $routes->male }}</b></p>
                  <p class="m-0">Female: <b>{{ $routes->female }}</b></p>
                  <p class="m-0">Total Passengers: <b>{{ ($routes->male + $routes->female) }}</b></p>
                </div>
                <div class="icon">
                  <i class="fas fa-map-marker-alt nav-icon"></i>
                </div>
                <a href="#" onclick="generateQr('{{ $routes->id }}', '{{ $routes->routes }}');" class="small-box-footer">View QR Code</a>
              </div>
            </div>
            @empty
          @endforelse
        </div>
        @endif
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('page-modals')
 <div class="modal fade" role="dialog" id="modal_qrcode">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title" >
          <h3 ><i class="fas fa-map-marker-alt nav-icon"></i> <span id="route_title"></span></h3>
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div id="trans_qrcode" class="text-center"></div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success" onclick="location.reload();">Done</button>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('js-scripts')
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<script>
   var qrcode = new QRCode("trans_qrcode", {
          text: 'test',
          width: 300,
          height: 300,
          colorDark : "#000000",
          colorLight : "#ffffff",
          correctLevel : QRCode.CorrectLevel.H
      });

    function generateQr(text, title){
      $("#route_title").text(title);
       qrcode.clear();
       qrcode.makeCode(text);
       $("div#trans_qrcode > img").removeAttr('style');
       $("#modal_qrcode").modal({'backdrop': 'static'});
    }



    function chart_render(){
      var donutData = {
      labels: [
          'Male',
          'Female',
      ],
      datasets: [
        {
          data: ['{{ getTransactionMaleFemale('male') }}','{{ getTransactionMaleFemale('female') }}'],
          backgroundColor : ['#007BFF', '#e85aa8'],
        }
      ]
    }

      var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
      var pieData        = donutData;
      var pieOptions     = {
        maintainAspectRatio : false,
        responsive : true,
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      new Chart(pieChartCanvas, {
        type: 'pie',
        data: pieData,
        options: pieOptions
      })
    }

    chart_render();


</script>

@endpush
