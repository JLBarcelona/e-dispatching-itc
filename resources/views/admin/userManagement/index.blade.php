@extends('admin.layout.app')
@section('title', Ucfirst($type).' Account Management')

@push('styles')

<style>
  .modal-size{
    max-width: 90% !important; 
    width: 65%;
  }
    @media only screen and (max-width: 768px) {
      .modal-size{
        max-width: 1000% !important; 
        width: 100%;
      }
    }

    /* Styles for devices with a maximum width of 480 pixels (e.g., smaller mobile phones) */
    @media only screen and (max-width: 480px) {
      .modal-size{
        max-width: 1000% !important; 
        width: 100%;
      }
    }
</style>

@endpush

@section('content')

  <form class="needs-validation" id="user_management_form" action="{{ route('admin.user_management.store') }}" novalidate>
    <div class="container-fluid">
      <div class="card d-none">
        <div class="card-header" onclick="$('#minimize_filter').click()" >
          <h5 class="card-title">Record Filter</h5>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
            
            </button>
          </div>
        </div>
        
        <div class="card-body">
          <div class="row">
            <div class="position-relative mb-2 col-md-4">
              <label>Fullname or Address</label>
              <input type="text" name="filter" id="filter" class="form-control" placeholder="Search">
              
            </div>
          </div>
          
        </div>
        <div class="card-footer text-right">
          <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
          <i class="fa fa-minus"></i> Minimize Filter
          </button>
          <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
          <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
        </div>
      </div>

      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
          <table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
        </div>
      </div>
    </div>
  </div>


  <!-- modal -->
    <div class="modal fade" role="dialog" id="modal_add_edit">
      <div class="modal-dialog modal-lg modal-size">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4">
              Add {{ Ucfirst($type) }} Account
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-4">
                <input type="hidden" name="type" id="type" value="{{ $type }}">
                <input type="hidden" name="id" id="id">
                <label class="required">First Name</label>
                <input type="text" name="first_name" id="first_name" placeholder="Enter firstname" class="mb-2 form-control" autocomplete="off">
              </div>
              <div class="col-sm-3">
                <label>Middle Name <small class="text-muted">(optional)</small></label>
                <input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename" class="mb-2 form-control" autocomplete="off">
              </div>
              <div class="col-sm-3">
                <label class="required">Last Name</label>
                <input type="text" name="last_name" id="last_name" placeholder="Enter lastname" class="mb-2 form-control" autocomplete="off">
              </div>
              <div class="col-sm-2">
                <label class="required">Suffix <small class="text-muted">(optional)</small></label>
                <!-- <input type="text" > -->
                <select name="suffix" id="suffix" placeholder="Jr." class="mb-2 form-control" autocomplete="off">
                  <option value=""></option> 
                  <option>SR</option> 
                  <option>JR</option> 
                  <option>II</option> 
                  <option>III</option> 
                  <option>IV</option> 
                  <option>V</option>
                  <option>VI</option>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="required">Sex</label>
                <select name="gender" id="gender" class="form-control" autocomplete="off">
                  <option selected="" disabled="">Please select gender</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="required">Date of Birth</label>
                <input type="date" name="birthdate" id="birthdate" class="mb-2 form-control" autocomplete="off">
              </div>
              <div class="col-sm-4">
                <label class="required">Contact Number</label>
                <input type="text" name="contact_number" id="contact_number" placeholder="Enter contact no." class="mb-2 form-control" autocomplete="off">
              </div>
               <div class="col-sm-12">
                <label class="required">Complete Address</label>
                <textarea class="form-control mb-2" rows="2" id="complete_address" name="complete_address" placeholder="Enter complete address"></textarea>
              </div>
              
              
              @if($type == 'driver')
                <div class="col-sm-6">
                    <label class="required">Email</label>
                    <input type="text" name="email_address" id="email_address" class="form-control mb-2" placeholder="Enter email">
                </div>

                <div class="col-sm-6">
                    <label class="required">Driver's license#</label>
                    <input type="text" name="driver_license_no" id="driver_license_no" class="form-control mb-2" placeholder="Enter driver's license no.">
                </div>

                <div class="col-sm-6">
                    <label class="required">License Expiration</label>
                    <input type="date" name="driver_license_expiration" id="driver_license_expiration" class="form-control mb-2" placeholder="Enter license expiration">
                </div>

                <div class="col-sm-6">
                    <label class="required">Conductor's Fullname (optional)</label>
                    <input type="text" name="conductor_name" id="conductor_name" class="form-control mb-2" placeholder="Enter Conductor's Fullname">
                </div>

                <div class="col-sm-6">
                    <label class="required">Contact Person</label>
                    <input type="text" name="contact_person" id="contact_person" class="form-control mb-2" placeholder="Enter Contact Person's Fullname">
                </div>
                <div class="col-sm-6">
                    <label class="required">Contact Person's Contact Number</label>
                    <input type="text" name="contact_person_cp_number" id="contact_person_cp_number" class="form-control mb-2" placeholder="Enter Contact Person's Contact number">
                </div>

                <div class="col-sm-12 mt-3"></div>

                <div class="col-sm-4">
                  <hr>
                </div>
                <div class="col-sm-4 text-center">
                  <h4>Vehicle Informations</h4>
                </div>
                <div class="col-sm-4">
                  <hr>
                </div>
              
                  <div class="col-sm-12">
                      <label class="required">Vehicle Model</label>
                      <input type="text" name="vehicle_model" id="vehicle_model" class="form-control mb-2" placeholder="Enter vehicle model">
                  </div>
                  <div class="col-sm-3">
                      <label class="required">Vehicle Type</label>
                      <select name="vehicle_type" id="vehicle_type" class="form-control form-select mb-2">
                        <option>Select Type</option>
                        <option>Bus</option>
                        <option>Van</option>
                        <option>Mini Bus</option>
                      </select>
                  </div>
                  <div class="col-sm-3">
                      <label class="required">Plate Number</label>
                      <input type="text" name="plate_number" id="plate_number" class="form-control mb-2" placeholder="Enter plate number">
                  </div>
                   <div class="col-sm-3">
                      <label class="required">Color</label>
                      <input type="text" name="color" id="color" class="form-control mb-2" placeholder="Enter color">
                  </div>
                  <div class="col-sm-3">
                      <label class="required">Capacity (Passengers)</label>
                      <input type="number" name="capacity" id="capacity" class="form-control mb-2" placeholder="Enter capacity">
                  </div>
                  
                  @else
                  <div class="col-sm-12">
                    <label class="required">Email</label>
                    <input type="text" name="email_address" id="email_address" class="form-control mb-2" placeholder="Enter email">
                </div>
                  
              @endif

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  </div>

   <div class="modal" tabindex="-1" role="dialog" id="user_details" data-bs-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">User Account</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Please take a screenshot or copy the user account.</p>
            <p>Username: <span id="username_account"></span></p>
            <p>Password: <span id="password_account"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
          </div>
        </div>
      </div>
    </div>
  <!-- modal -->
@endsection

@push('page-modals')

@endpush

@push('js-scripts')
<script type="text/javascript">

  $("#user_management_form").on('submit', function(e){
    var mydata = $(this).serialize();
    let url = $(this).attr('action');
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
      type:"post",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_submit').prop('disabled', true);
          $('#btn_submit').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          });
          
          validation('user_management_form', response.errors);
          show_user_accounts();
          $('#modal_add_edit').modal('hide');


          $("#username_account").text(response.details.username);
          $("#password_account").text(response.details.password);

          if (response.details !== false) {
            $("#user_details").modal('show');
          }
        }else{
          validation('user_management_form', response.errors);
        }
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Save');
      },
      error:function(error){
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Save');
        console.log(error);
      }
    });
  });


  $('#refresh-filter').on('click', function(){
    $('#filter').val('');
    submit_filter();
  });

  function submit_filter(){
    show_user_accounts();
    
  }

  show_user_accounts();
  var tbl_user_accounts;
  function show_user_accounts(){
    if (tbl_user_accounts) {
      tbl_user_accounts.destroy();
    }
      tbl_user_accounts = $('#tbl_user_accounts').DataTable({
      dom: 'Bfrtip',
      dom: "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
          buttons: [
              {
                  text: 'Add {{ Ucfirst($type) }} Account',
                  className: "btn btn-info",
                  action: function ( e, dt, node, config ) {
                      $('#modal_add_edit').modal('show');
                      add_user_accounts();
                  }
              }
          ],
      language: {
      "emptyTable": "No data available"
      },
    ajax: {
          url: "{{ route('admin.user_management.list', $type) }}",
          data: {filter : $('#filter').val()},
      },
      columns: [{
      className: '',
      "data": "fullname",
      "title": "Fullname",
      "orderable": false,
    },{
      className: '',
      "data": "gender",
      "title": "Sex",
      "orderable": false,
    },{
      className: '',
      "data": "birthdate",
      "title": "Date of Birth",
      "orderable": false,
    },{
      className: '',
      "data": "contact_number",
      "title": "Contact #",
      "orderable": false,
    },{
      className: '',
      "data": "complete_address",
      "title": "Complete Address",
      "orderable": false,
    },{
      className: '',
      "data": "email_address",
      "title": "Email",
      "orderable": false,
    },
    @if($type == 'driver')
    {
      className: '',
      "data": "driver_license_no",
      "title": "License #",
      "orderable": false,
    },{
      className: '',
      "data": "driver_license_expiration",
      "title": "License Expiration",
      "orderable": false,
    },{
      className: '',
      "data": "contact_person",
      "title": "Contact Person",
      "orderable": false,
    },{
      className: '',
      "data": "contact_person_cp_number",
      "title": "Contact Person's CP#",
      "orderable": false,
    },{
      className: '',
      "data": "plate_number",
      "title": "Plate #",
      "orderable": false,
    },{
      className: '',
      "data": "vehicle_model",
      "title": "Model",
      "orderable": false,
    },{
      className: '',
      "data": "color",
      "title": "Color",
      "orderable": false,
    },{
      className: '',
      "data": "vehicle_type",
      "title": "Type",
      "orderable": false,
    },{
      className: '',
      "data": "conductor_name",
      "title": "Conductor Name",
      "orderable": false,
    },
    @endif
    {
      className: 'text-center',
      "data": "id",
      "title": "Options",
      "orderable": false,
      "render": function(data, type, row, meta){
        newdata = '<button type="button" class="btn btn-sm btn-info mb-2" style="width:50px;" onclick="edit_user('+data+')"><i class="fa fa-edit"></i></button>\
           <button type="button" class="btn btn-sm btn-danger mb-2" style="width:50px;" onclick="delete_user('+data+')"><i class="fa fa-trash"></button>'
      return newdata;
      }
    }
    ]
    });
  }


  function edit_user(id){
    Swal.fire({
        title: 'Are you sure!',
        text: "Do you want to edit this user information?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
          headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}",
                    },
                type:"get",
                url:"{{ route('admin.user_management.find') }}/"+id,
                data:{},
                dataType:'json',
                beforeSend:function(){
                },
                success:function(response){
                 if (response.status == true) {
                  $('#hide_on_edit').hide();
                    $('#first_name').val(response.data.first_name);
                    $('#middle_name').val(response.data.middle_name);
                    $('#last_name').val(response.data.last_name);
                    $('#gender').val(response.data.gender);
                    $('#birthdate').val(response.data.birthdate);
                    $('#vehicle').val(response.data.vehicle);
                    $('#complete_address').val(response.data.complete_address);
                    $('#contact_number').val(response.data.contact_number);
                    $('#email_address').val(response.data.email_address);
                    $('#driver_license_no').val(response.driver_license_no);
                    $('#driver_license_expiration').val(response.driver_license_expiration);
                    $('#conductor_name').val(response.conductor_name);
                    $('#id').val(response.data.id);
                    $('#driver_license_no').val(response.data.driver_license_no);
                    $('#driver_license_expiration').val(response.data.driver_license_expiration);
                    $('#conductor_name').val(response.data.conductor_name);
                    $('#contact_person').val(response.data.contact_person);
                    $('#contact_person_cp_number').val(response.data.contact_person_cp_number);
                    $('.modal-title').html('Edit User');
                    $('#modal_add_edit').modal('show');
                    
                 }else{
                  console.log(response);
                 }
                },
                error: function(error){
                  console.log(error);
                }
              });
        }
      })
   
  }


  function delete_user(id){

    Swal.fire({
        title: 'Are you sure?',
        text: "This user will be deleted!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
        type:"delete",
        url:"{{ route('admin.user_management.destroy')}}/"+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          show_user_accounts();
         
           
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

          Swal.fire(
            'Deleted!',
            'User has been deleted.',
            'success'
          )
        }
      })

    
  }

  

  function add_user_accounts(){
    $('#hide_on_edit').show();
    $('#user_management_form').trigger("reset");
    $('.modal-title').html('Add {{ Ucfirst($type) }} Account');
    $('#modal_add_edit').modal('show');
  }

  
</script>
@endpush
