@extends('admin.layout.app')
@section('title', 'Vehicle Management')

@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <div class="container-fluid">
      <div class="card d-none">
        <div class="card-header" onclick="$('#minimize_filter').click()" >
          <h5 class="card-title">Record Filter</h5>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
            
            </button>
          </div>
        </div>
        
        <div class="card-body">
          <div class="row">
            <div class="position-relative mb-2 col-md-4">
              <label>Fullname or Address</label>
              <input type="text" name="filter" id="filter" class="form-control" placeholder="Search here">
              
            </div>
          </div>
          
        </div>
        <div class="card-footer text-right">
          <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
          <i class="fa fa-minus"></i> Minimize Filter
          </button>
          <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
          <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
        </div>
      </div>

      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
          <table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
        </div>
      </div>
    </div>
  </div>


  <!-- modal here -->
    <form class="needs-validation" id="vehicle_form" action="{{ route('admin.vehicles.save') }}" novalidate>
      <div class="modal fade" tabindex="-1" role="dialog" id="modal_vehicle_form" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modal_vehicle_form_title">Add Vehicles</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                @csrf
                <input type="hidden" name="id" id="id" />
                <div class="col-md-12 mb-2">
                  <label>Color</label>
                  <input type="text" name="color" id="color" class="form-control " required  Enter color/>
                </div>
                <div class="col-md-12 mb-2">
                  <label>Vehicle model</label>
                  <input type="text" name="vehicle_model" id="vehicle_model" class="form-control " required  Enter vehicle model/>
                </div>
                <div class="col-md-12 mb-2">
                  <label>Plate number</label>
                  <input type="text" name="plate_number" id="plate_number" class="form-control " required  Enter plate number/>
                </div>
                <div class="col-md-12 mb-2">
                  <label>Type</label>
                  <select name="type" id="type" class="form-control " required>
                    <option selected="" disabled="">Please select vehicle type</option>
                    @foreach($vehicles as $key => $row)
                    <option value="{{ $key }}">{{ ucfirst($row) }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-success" id="vehicle_form_btn" >Save</button>
            </div>
          </div>
        </div>
      </div>
    </form>

   <div class="modal" tabindex="-1" role="dialog" id="user_details" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">User Account</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Please take a screenshot or copy the user account.</p>
            <p>Username: <span id="username_account"></span></p>
            <p>Password: <span id="password_account"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success">Ok</button>
          </div>
        </div>
      </div>
    </div>
  <!-- modal here -->
@endsection

@push('page-modals')

@endpush

@push('js-scripts')
<script type="text/javascript">

  // $("#user_management_form").on('submit', function(e){
  //   var mydata = $(this).serialize();
  //   let url = $(this).attr('action');
  //   e.stopPropagation();
  //   e.preventDefault(e);

  //   $.ajax({
  //     headers: {
  //                   'X-CSRF-TOKEN': "{{csrf_token()}}",
  //               },
  //     type:"post",
  //     url:url,
  //     data:mydata,
  //     cache:false,
  //     beforeSend:function(){
  //         //<!-- your before success function -->
  //         $('#btn_submit').prop('disabled', true);
  //         $('#btn_submit').text('Please wait...');
  //     },
  //     success:function(response){
  //         //console.log(response)
  //       if(response.status == true){
  //         Swal.fire({
  //                   position: 'center',
  //                   icon: 'success',
  //                   title: response.message,
  //                   showConfirmButton: false,
  //                   timer: 1500
  //                 })
          
  //         validation('user_management_form', response.errors);
  //         show_user_accounts();
  //         $('#modal_add_edit').modal('hide');


  //         $("#username_account").text(response.details.username);
  //         $("#password_account").text(response.details.password);
  //         $("#user_details").modal('show');


  //       }else{
  //         validation('user_management_form', response.errors);
  //       }
  //       $('#btn_submit').prop('disabled', false);
  //       $('#btn_submit').text('Save');
  //     },
  //     error:function(error){
  //       $('#btn_submit').prop('disabled', false);
  //       $('#btn_submit').text('Save');
  //       console.log(error);
  //     }
  //   });
  // });

  $("#vehicle_form").on('submit', function(e){
    e.preventDefault();
    let id = $('#id').val();
    let url = $(this).attr('action');
    let formData = $(this).serialize();
    $.ajax({
      type:"POST",
      url:url+'/'+id,
      data:formData,
      dataType:'json',
      beforeSend:function(){
        $('#vehicle_form_btn').prop('disabled', true);
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          $('#modal_vehicle_form').modal('hide');
           Swal.fire({
            position: 'center',
            icon: 'success',
            title: response.message,
            showConfirmButton: false,
            timer: 1500
          });
        show_user_accounts();
        }else{
          console.log(response);
        }
          validation('vehicle_form', response.error);
          $('#vehicle_form_btn').prop('disabled', false);
      },
      error: function(error){
        $('#vehicle_form_btn').prop('disabled', false);
        console.log(error);
      }
    });
  });

  function add_vehicles(){
    $("#id").val('');
    $('#color').val('');
    $('#vehicle_model').val('');
    $('#plate_number').val('');
    $('#type').val('');
    $('.modal-title').html('Add Vehicle');
    $("#modal_vehicle_form").modal('show');
  }


  function edit_vehicles(id){
    $.ajax({
      type:"GET",
      url:"{{ route('admin.vehicles.find') }}/"+id,
      data:{},
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          $('#id').val(response.data.id);
          $('#color').val(response.data.color);
          $('#vehicle_model').val(response.data.vehicle_model);
          $('#plate_number').val(response.data.plate_number);
          $('#type').val(response.data.type);
          $('.modal-title').html('Edit Vehicle');
          $('#modal_vehicle_form').modal('show');
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
    });
  }

  $('#refresh-filter').on('click', function(){
    $('#filter').val('');
    submit_filter();
  });

  function submit_filter(){
    show_user_accounts();
    
  }

  show_user_accounts();
  var tbl_user_accounts;
  function show_user_accounts(){
    if (tbl_user_accounts) {
      tbl_user_accounts.destroy();
    }
      tbl_user_accounts = $('#tbl_user_accounts').DataTable({
      dom: 'Bfrtip',
      dom: "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
          buttons: [
          ],
      language: {
      "emptyTable": "No data available"
      },
    ajax: {
          url: "{{ route('admin.vehicles.list') }}",
          data: {filter : $('#filter').val()},
      },
      columns: [{
      className: '',
      "data": "fullname",
      "title": "Fullname",
      "orderable": false,
    },{
      className: '',
      "data": "plate_number",
      "title": "Plate #",
      "orderable": false,
    },{
      className: '',
      "data": "vehicle_model",
      "title": "Model",
      "orderable": false,
    },{
      className: '',
      "data": "color",
      "title": "Color",
      "orderable": false,
    },{
      className: '',
      "data": "vehicle_type",
      "title": "Type",
      "orderable": false,
    }
    ]
    });
  }




  function delete_vehicle(id){

    Swal.fire({
        title: 'Are you sure?',
        text: "This user will be deleted!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
        type:"delete",
        url:"{{ route('admin.vehicles.delete') }}/"+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          show_user_accounts();
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

          Swal.fire(
            'Deleted!',
            'User has been deleted.',
            'success'
          )
        }
      })

    
  }

  
</script>
@endpush
