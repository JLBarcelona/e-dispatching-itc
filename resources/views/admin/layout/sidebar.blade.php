 <!-- Main Sidebar Container old sidebar-dark-primary -->
  <aside class="main-sidebar sidebar-dark-primary elevation-1">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{ asset('img/stgo.png') }}" alt="Company Logo" class="brand-image img-circle">
      <span class="brand-text font-weight-bold">E-Dispatching</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar text-sm">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
           <a href="">
             <img src="{{ Auth::user()->getAvatarImg() }}" class="img-circle elevation-2" alt="Employee Male Default">
           </a>
        </div>
        <div class="info">
          <a href="" class="d-block"><b>{{ auth()->user()->fullname }} <span class="badge badge-primary">{{ auth()->user()->getTypeText(auth()->user()->type) }}</span></b></a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-compact" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="{{ route('admin.home') }}" class="nav-link {{ (request()->is('dashboard'))? 'active': '' }}">
                <i class="fa fa-th nav-icon"></i>
                <p>Dashboard</p>
              </a>
            </li>

            @if(auth()->user()->type === 1)
            <li class="nav-item {{ (request()->is('dashboard/users/manage/dispatcher') || request()->is('dashboard/users/manage/monitor') || request()->is('dashboard/users/manage/driver'))? 'menu-open' : '' }}">
              <a href="#" class="nav-link {{ (request()->is('dashboard/users/manage/dispatcher') || request()->is('dashboard/users/manage/monitor') || request()->is('dashboard/users/manage/driver'))? 'active' : '' }}">
                <i class="nav-icon far fa-user"></i>
                <p>
                  User Accounts
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview ml-3" style="{{ (request()->is('dashboard/users/manage/dispatcher') || request()->is('dashboard/users/manage/monitor') || request()->is('dashboard/users/manage/driver'))? '' : 'display: none;' }} ">
                <li class="nav-item">
                  <a href="{{ route('admin.user_management.index', 'dispatcher') }}" class="nav-link {{ (request()->is('dashboard/users/manage/dispatcher'))? 'active': '' }}">
                    <i class="fa fa-people-arrows nav-icon"></i>
                    <p>Dispatcher</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('admin.user_management.index', 'monitor') }}" class="nav-link {{ (request()->is('dashboard/users/manage/monitor'))? 'active': '' }}">
                    <i class="fa fa-user-cog nav-icon"></i>
                    <p>Monitor</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('admin.user_management.index', 'driver') }}" class="nav-link {{ (request()->is('dashboard/users/manage/driver'))? 'active': '' }}">
                    <i class="far fa-id-card nav-icon"></i>
                    <p>Driver</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="{{ route('admin.vehicles.index') }}" class="nav-link {{ (request()->is('dashboard/vehicles'))? 'active': '' }}">
                <i class="fas fa-bus nav-icon"></i>
                <p>Vehicle</p>
              </a>
            </li>
            @endif
            @if(auth()->user()->type !== 4 && auth()->user()->type !== 1)
              <li class="nav-item">
                <a href="{{ route('admin.scanner') }}" class="nav-link {{ (request()->is('dashboard/scan'))? 'active': '' }}">
                  <i class="fas fa-qrcode nav-icon"></i>
                  <p>Scanner</p>
                </a>
              </li>
            @endif

            <li class="nav-item">
              <a href="{{ route('admin.transaction.index') }}" class="nav-link {{ (request()->is('dashboard/transactions'))? 'active': '' }}">
                <i class="fas fa-list nav-icon"></i>
                <p>Monitoring</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('admin.profile_settings.index') }}" class="nav-link {{ (request()->is('dashboard/profile-settings'))? 'active': '' }}">
                <i class="fas fa-cog nav-icon"></i>
                <p>Account Settings</p>
              </a>
            </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>