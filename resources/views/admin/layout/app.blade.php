<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title')</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/stgo.png') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-new/datatables.min.css') }}"/>

    <!-- Lada Button -->
    <link type="text/css" href="{{ asset('plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
      <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
  
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <style type="text/css">
      input::-webkit-outer-spin-button,
      input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }

      /* Firefox */
      input[type=number] {
        -moz-appearance: textfield;
      }


    </style>
    @stack('styles')    
    <!-- CUSTOM CSS FILES HERE -->
</head>
    
<body class="hold-transition sidebar-mini sidebar-collapse">

<div class="wrapper">

  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="" src="{{ asset('img/stgo.png') }}" alt="AdminLTELogo" height="150" width="150">
  </div>

    @include('admin.layout.navbar')

    @include('admin.layout.sidebar')

    <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">
        @include('admin.layout.breadcrumb')
        @yield('content')
      </div>

    <!-- /.content-wrapper -->
    @include('admin.layout.back_to_top')
    @include('admin.layout.footer')

</div>

<!-- Global Modals -->
@stack('page-modals')
<!-- End Global Modals -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- ladda -->
<script src="{{ asset('plugins/ladda/spin.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('plugins/ladda/ladda.jquery.min.js') }}"></script>

<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('plugins/datatables-new/datatables.min.js') }}"></script>

<!-- SOLUTION FOR EXPORT COMPLEX HEADERS COMMENT IF SOMETHING GOES WRONG - RAVEN -->
<script src="{{ asset('plugins/datatables-new/Buttons-2.2.3/js/dataTables.buttons.js') }}"></script>
<script src="{{ asset('plugins/datatables-new/Buttons-2.2.3/js/buttons.html5.js') }}"></script>
<script src="{{ asset('plugins/datatables-new/Buttons-2.2.3/js/buttons.print.js') }}"></script>
<!-- SOLUTION FOR EXPORT COMPLEX HEADERS COMMENT IF SOMETHING GOES WRONG - RAVEN -->

<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- Custom Js -->
<script src="{{ asset('custom/validation.js') }}"></script>
<script src="{{ asset('custom/qr.js') }}"></script>
<script src="{{ asset('custom/scanner.js') }}"></script>

<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<script>

    var Toast = Swal.mixin({
      toast: true,
      position: 'toast-top-center',
      showConfirmButton: false,
      timer: 3000
    });

  class custom_err{
      static clear(form_object){//call this using custom_err.clear(form_object)
          form_object.find('input, textarea, select, div, checkbox').removeClass('is-invalid');
      }


      //use this when you want to use the loop here directly WE USE THIS BY DEFAULT
      static display_all(errors=[],form_object){
        $.each(errors, function(key, value) {
            custom_err.display(form_object, key, value[0]);
        });
      }

      //use this when you're using the loop in error condition WE USE THIS WHEN WE WANT TO ALTER SOMETHING
      static display(form_object, field_name, error_message){//call this using custom_err.display(form_object, field_name, error_message)
          form_object.find('[name="'+field_name+'"]').addClass('is-invalid');
          form_object.find('[name="'+field_name+'"]').parent().find('.invalid-feedback').text(error_message);
      }
      
  }

  $(document).on('select2:open', (e) => {
    const selectId = e.target.id
      $(".select2-search__field[aria-controls='select2-" + selectId + "-results']").each(function (
          key,
          value,
      ){
          value.focus();
      })
  })

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};

</script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
@stack('js-scripts') 
</body>
</html>
