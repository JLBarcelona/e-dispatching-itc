@extends('user.layout.app')
@section('title', 'Dashboard')

@push('styles')

<style>
    
</style>

@endpush

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3></h3>
                <p>バー</p>
              </div>
              <div class="icon">
                <i class="fas fa-warehouse nav-icon"></i>
              </div>
              <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3></h3>
                <p>スタッフ</p>
              </div>
              <div class="icon">
                <i class="fas fa-users nav-icon"></i>
              </div>
              <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6 d-none">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>100</h3>
                <p>販売</p>
              </div>
              <div class="icon">
                <i class="fas fa-landmark nav-icon"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

        </div>


        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('page-modals')

@endpush

@push('js-scripts')

<script>
   
    
</script>

@endpush
