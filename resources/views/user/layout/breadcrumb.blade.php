<section class="content-header breadcrumb sidebar-light-primary">
    <div class="container-fluid">
      <div class="row breadcrumb-row-content">
        <div class="col-sm-6">
          <h1 class="m-0">{!! View::getSection('title') !!}</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
            <li class="breadcrumb-item active">{{ View::getSection('title') }}</li>
          </ol>
        </div>
      </div>
    </div>
</section>