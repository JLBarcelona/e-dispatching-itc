 <!-- Main Sidebar Container old sidebar-dark-primary -->
  <aside class="main-sidebar sidebar-dark-olive elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{ asset('dist/img/logo.png') }}" alt="Company Logo" class="brand-image img-circle">
      <span class="brand-text font-weight-bold">{{ config('app.name') }}</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar text-sm">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
           <a href="">
             <img src="{{ asset('dist/img/avatar5.png') }}" class="img-circle elevation-2" alt="Employee Male Default">
           </a>
        </div>
        <div class="info">
          <a href="" class="d-block">User</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-compact" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="" class="nav-link {{ (request()->is('admin/home'))? 'active': '' }}">
                <i class="fas fa-columns nav-icon"></i>
                <p>Dashboard</p>
              </a>
            </li>

            

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>