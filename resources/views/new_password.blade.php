<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forgot Password</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <style type="text/css">
       
    </style>
</head>
<body class="hold-transition login-page">


<div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">

            <a href="javascript:;" class="h4">Password Reset</a>
        </div>
        <div class="card-body mt-1">
            @if(session('status'))
                <label class="text-center text-danger">  {{ session('status') }} </label>
            @endif
            
            <form id="newPass_form" method="POST" action="{{ route('auth.update.password') }}" novalidate>
                @csrf
                <input value="{{$token}}" id="token" name="token" hidden>
                @if (Session::has('message'))
                    <div class="alert alert-default-danger"><span><i class="fas fa-exclamation-circle"></i></span> {{ Session::get('message') }}</div>
                @endif
                <div class="input-group">
                    <input type="password" class="form-control" placeholder="New password" id="password" name="password" value="">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <label class="text-danger text-sm mb-3">  {{ json_decode(session('errors'), true)['password'][0] ?? '' }} </label>

                <div class="input-group">
                    <input type="password" class="form-control" placeholder="Re-enter password" id="confirm_password" name="confirm_password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <label class="text-danger text-sm mb-3">  {{ json_decode(session('errors'), true)['confirm_password'][0] ?? '' }} </label>

                <div class="row mt-3">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                    </div>
                </div>

            </form>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>


</body>
</html>
