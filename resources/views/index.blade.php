@if(auth()->check())
    <script type="text/javascript">
        window.location = "{{ url('dashboard') }}";
    </script>
@else @endif
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="javascript:;" class="h3">E-Despatching <br> Smart Tranportation</a>
    </div>
    <div class="card-body mt-1">
      @if(session('status'))
           <label class="text-center text-danger">  {{ session('status') }} </label>
      @endif
      @if(session('change_pass'))
           <label class="text-center text-success"><span><i class="far fa-check-circle"></i></span>  {{ session('change_pass') }} </label>
      @endif
      <form id="login_form" method="POST" action="{{ route('auth.authenticate') }}" novalidate>
        @csrf
        <div class="input-group">
          <input type="text" class="form-control" name="username" placeholder="Email address">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
         <label class="text-danger text-sm mb-3">  {{ json_decode(session('errors'), true)['username'][0] ?? '' }} </label>

        <div class="input-group">
          <input type="password" class="form-control" name="password" id="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-eye" onclick="password_toggler(this, '#password');"></span>
            </div>
          </div>
        </div>
        <label class="text-danger text-sm">  {{ json_decode(session('errors'), true)['password'][0] ?? '' }} </label>
        <div>
        <a href="{{ route('auth.forget.password') }}">Forget Password?</a>
        </div>
        <div class="row mt-3">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
          </div>
          <!-- /.col -->
        </div>

      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<script type="text/javascript">
    function password_toggler(_this, selector){
      $(_this).toggleClass('fa-eye fa-eye-slash')
      $(selector).attr('type', function(index, attr){
          return attr == 'text' ? 'password' : 'text';
      });
    }
</script>

</body>
</html>
