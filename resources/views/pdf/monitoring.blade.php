<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style>
        /* Add any additional styling here */
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: center;
        }

        img {
            max-width: 100px;
            max-height: 100px;
        }

        .center {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="center">
        <img src="{{ $logo }}" alt="Logo">
        <h1 style="margin: 2px;">{{ $title }}</h1>
        <p style="margin: 2px;"> <b>CITY OF SANTIAGO</b></p>
        <p style="margin: 2px;"> <b>{{ $content }}</b></p>
    </div>

    <table>
        <thead>
            <tr>
                <th colspan="2">Date: {{ date('Y-m-d') }}</th>
                <th colspan="7" class="center">DISPATCHER REPORT</th>
                <th colspan="1">SHIFT:</th>
            </tr>
            <tr>
                <th>No.</th>
                <th>Name of Driver</th>
                <th>Plate No.</th>
                <th>Route</th>
                <th>No. of Passengers</th>
                <th>Time of Arrival</th>
                <th>Time of Departure</th>
                <th>Male Passengers</th>
                <th>Female Passengers</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            @php $i = 1; @endphp
            @foreach($tdata as $dt)
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $dt->driver->fullname }}</td>
                    <td>{{ $dt->driver->plate_number }}</td>
                    <td>{{ $dt->routes }}</td>
                    <td>{{ (intval($dt->male) + intval($dt->female))  }}</td>
                    <td>{{ $dt->arrival_time }}</td>
                    <td>{{ $dt->departure_time }}</td>
                    <td>{{ $dt->male }}</td>
                    <td>{{ $dt->female }}</td>
                    <td>{{ $dt->status_text }}</td>
                </tr>
                @php $i++ @endphp
            @endforeach
        </tbody>
    </table>

    <p>Reported By: {{ auth()->user()->fullname }}</p>
</body>
</html>