<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Staff\HomeController;


/* Admin Routes */
Route::name('staff.')->middleware(['auth','role:staff'])->group(function () {
	Route::get('/', function(){return Redirect::to('staff/home');});
    Route::get('home', [HomeController::class, 'index'])->name('home');	
});
?>