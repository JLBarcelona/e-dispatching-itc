<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Driver\HomeController;



/* Admin Routes */
Route::name('driver.')->middleware(['auth','role:driver'])->group(function () {
	
	Route::get('/', function(){return Redirect::to('driver/home');});
    Route::get('home', [HomeController::class, 'index'])->name('home');

	
});
?>