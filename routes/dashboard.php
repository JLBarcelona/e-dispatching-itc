<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\userManagementController;
use App\Http\Controllers\Admin\profileSettingsController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\VehicleController;
use App\Http\Controllers\Admin\TransactionController;

/* Admin Routes */
Route::name('admin.')->middleware(['auth','role:admin'])->group(function () {
	
    Route::get('', [HomeController::class, 'index'])->name('home');
    Route::get('scan', [HomeController::class, 'scanner'])->name('scanner');
    Route::get('scan-code', [HomeController::class, 'scanCode'])->name('scanning');
 
    Route::group(['prefix' => 'users', 'as' => 'user_management.'], function(){
		Route::controller(userManagementController::class)->group(function () {
			Route::get('/manage/{type}', 'index')->name('index');
			Route::post('store', 'store')->name('store');
			Route::get('list/{type}', 'list')->name('list');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('destroy/{id?}', 'destroy')->name('destroy');
			Route::post('/upload', 'uploadFile')->name('upload.file');
		});
	});

	Route::group(['prefix' => 'profile-settings', 'as' => 'profile_settings.'], function(){
		Route::controller(profileSettingsController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::post('change_password', 'change_password')->name('change_password');
			Route::post('store', 'store')->name('store');
		});
	});

	Route::group(['prefix' => 'vehicles', 'as' => 'vehicles.'], function(){
		Route::controller(VehicleController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
		});
	});


	Route::group(['prefix' => 'transactions', 'as' => 'transaction.'], function(){
		Route::controller(TransactionController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::get('download', 'generatePDF')->name('pdf');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');

		});
	});

	
});
?>