<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\MonthySummaryController;
use App\Http\Controllers\Admin\YearlySummaryController;
use App\Http\Controllers\Admin\DailyReportController;
use App\Http\Controllers\Admin\StaffController;
use App\Http\Controllers\Admin\BranchController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ShoppingController;
use App\Http\Controllers\Admin\SalesController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\UserManagementController;


/* Admin Routes */
Route::name('admin.')->middleware(['auth','role:admin'])->group(function () {
	
	Route::get('/', function(){return Redirect::to('admin/home');});
    Route::get('home', [HomeController::class, 'index'])->name('home');

	Route::name('profile.')->group(function () {
	    Route::controller(UserController::class)->group(function () {
		    Route::get('/profile', 'index')->name('index');
		    Route::post('/profile', 'update')->name('update');
		    Route::post('/profile/update-password', 'updatePassword')->name('update-password'); 
		});
	});

	Route::name('monthly_summary.')->group(function () {
	    Route::controller(MonthySummaryController::class)->group(function () {
		    Route::get('monthly-summary/', 'index')->name('index'); 
		});
	});

	Route::name('yearly_summary.')->group(function () {
	    Route::controller(YearlySummaryController::class)->group(function () {
		    Route::get('yearly-summary/', 'index')->name('index'); 
		});
	});
    
    Route::name('daily_report.')->group(function () {
        Route::controller(DailyReportController::class)->group(function () {
            Route::get('daily-report/', 'index')->name('index');
            Route::get('daily-report/get-data/{bd_id?}/{bar_id?}', 'getReport')->name('getReport');
            Route::get('daily-report/get-data/get-date/{bd_id?}/{bar_id?}', 'getDate')->name('getDate');
            Route::post('daily-report/save', 'save')->name('save');
            Route::delete('daily-report/delete/{id?}', 'delete')->name('delete')->whereNumber('id');
            Route::get('daily-report/get-daily-salary', 'getDailySalaries')->name('getDailySalaries');

            Route::get('daily-report/get-daily-summary', 'getDailySummary')->name('daily.summary');
            Route::get('daily-report/get-daily-customers', 'getDailyCustomers')->name('daily.customers');

            Route::get('daily-report/get-shopping', 'getShoppingData')->name('daily.shopping');
            Route::get('daily-report/get-totals', 'getTotals')->name('daily.totals');

        });
    });
    
    Route::name('staff.')->group(function () {
        Route::controller(StaffController::class)->group(function () {
            Route::get('staff/', 'index')->name('index');
            Route::get('staff/get-data/', 'getData')->name('list');
            Route::get('staff/edit/{id?}', 'edit')->name('edit')->whereNumber('id');
            Route::delete('staff/delete/{id?}', 'destroy')->name('destroy')->whereNumber('id');
        });
    });

	Route::name('branch.')->group(function () {
        Route::controller(BranchController::class)->group(function () {
			Route::get('branch/','index')->name('index');
		    Route::get('branch/get-data/', 'getData')->name('list');
            Route::get('branch/edit/{id?}', 'edit')->name('edit')->whereNumber('id');
		    Route::post('branch/store/', 'store')->name('store');
            Route::delete('branch/delete/{id?}', 'destroy')->name('destroy')->whereNumber('id');
        });
    });

	Route::name('shopping.')->group(function () {
        Route::controller(ShoppingController::class)->group(function () {
            Route::get('shopping/', 'index')->name('index');
            Route::post('shopping/save', 'save')->name('save');
            Route::get('shopping/list', 'list')->name('list');
            Route::post('shopping/edit/{id?}', 'edit')->name('edit')->whereNumber('id');
            Route::delete('shopping/delete/{id?}', 'delete')->name('delete')->whereNumber('id');
        });
    });

    Route::name('sales.')->group(function () {
        Route::controller(SalesController::class)->group(function () {
            Route::post('sales/save_update', 'saveUpdate')->name('save.update');
            Route::get('sales/display-data', 'displayData')->name('display.data');
            Route::get('sales/display-details', 'displayDetails')->name('display.details');
            Route::post('sales/details-save', 'saveUpdateDetails')->name('save.update.details');
            Route::delete('sales/sales-detail-delete/{id?}', 'deleteSaleDetail')->name('delete.details')->whereNumber('id');
        });
    });

    Route::name('company.')->group(function () {
        Route::controller(CompanyController::class)->group(function () {
            Route::get('company/', 'index')->name('index');
            Route::post('company/store', 'store')->name('store');
            Route::get('company/fetch', 'fetch')->name('fetch');
           
        });
    });

        Route::name('user_management.')->group(function () {
        Route::controller(UserManagementController::class)->group(function () {
            Route::get('user_management/','index')->name('index');
            Route::get('user_management/get-data/', 'getData')->name('list');
            Route::get('user_management/edit/{id?}', 'edit')->name('edit')->whereNumber('id');
            Route::post('user_management/store/', 'store')->name('store');
            Route::delete('user_management/delete/{id?}', 'destroy')->name('destroy')->whereNumber('id');
        });
    });
});
?>