<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Admin\StaffController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');




//Authentications
Route::group(['prefix'=> 'auth'], function(){

    Route::name('auth.')->group(function () {
        // For Login
        Route::controller(AuthController::class)->group(function () {
             Route::post('request', 'authenticate')->name('authenticate');
             Route::get('forget-password', 'forget_password')->name('forget.password');
             Route::post('forget-password/submit', 'forget_password_submit')->name('forget.password.submit');
             Route::get('change-password/{token?}', 'change_password')->name('change.password');
             Route::post('change-password/submit', 'change_password_submit')->name('change.password.submit');
        });

        // // For Forgot Password
        // Route::controller(ForgotPasswordController::class)->group(function () {
        //     Route::get('forget-password', 'index')->name('forget.password.get');
        //     Route::post('forget-password','store')->name('forget.password.post');
        //     Route::get('reset-password/{token}', 'showResetPasswordForm')->name('reset.password.get');
        //     Route::post('reset-password', 'submitResetPasswordForm')->name('reset.password.post');
        //     Route::get('reset-password-success/', 'resetSuccess')->name('reset.password.success');
        //     Route::get('reset-password-sent/', 'sentResetPasswordPage')->name('reset.password.sent');
        // });
        
    });
}); 


Route::get('/staff/create', [StaffController::class, 'create'])->name('staff.create');
Route::post('/staff', [StaffController::class, 'store'])->name('staff.store');
Route::PUT('/staff', [StaffController::class, 'store'])->name('staff.store');
Route::get('/get-staff', [StaffController::class, 'getStaffList'])->name('staff.list');


?>


