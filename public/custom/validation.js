function validation(form, err){
	let inputs = $('#'+form+' .form-control');
	inputs.each(function(index){
		let id = $(this).attr('id');
    	$("#"+id).removeClass('is-invalid');
    	let exist = document.getElementById('err_'+id);
    	let error = Object.assign({}, err);

    	if (Object.hasOwn(error, id)) {
    		let message = (err[id] !== 'undefined')? err[id] : '';
    		if (exist) {
				$('#err_'+id).text(message);
        	}else{
        		$("#"+id).parent().append('<div class="invalid-feedback" id="err_'+id+'">'+ message+'</div>');
        	}
    		$("#"+id).addClass('is-invalid');
    	}else{
    		$("#"+id).removeClass('is-invalid');
    	}
	});

  validateInput();
}

$(document).ready(function(){
  $("form.needs-validation :input").on('input', function(){
      var _this = $(this);
      var checkClass = _this.hasClass('is-invalid');
      if (checkClass) {
        _this.removeClass('is-invalid');
      }
  });
});

function validateInput(){
    $(".form-control").each(function(e){
      $(this).on('input', function(){
        var _this = $(this);
          var checkClass = _this.hasClass('is-invalid');
          if (checkClass) {
            _this.removeClass('is-invalid');
          }
      });
    });
}

function handleFileSelect(e, selector) {
    var files = e.target.files;
    var filesArr = Array.prototype.slice.call(files);
    filesArr.forEach(function (f) {
      if (!f.type.match("image.*")) {
        return;
      }

      var reader = new FileReader();
      reader.onload = function (e) {
        if (selector.data('preview-type') === 'background') {
           selector.attr('style', 'background-image:url(\''+e.target.result+'\')');
        }else{
           selector.attr('src', e.target.result);
        }
         selector.attr('data-file', f.name);
      };
      reader.readAsDataURL(f);
    });
  }

 $(".preview-upload").each(function(e){
    $(this).on('change', function(event){
      let selector = $("#"+$(this).data('preview-selector'));
      handleFileSelect(event, selector);
    });
  });