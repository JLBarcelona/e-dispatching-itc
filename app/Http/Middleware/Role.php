<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Page;
use App\Models\RolePage;


class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role)
    {
        $user = Auth::user();
        $route_name = \Request::route()->getName();
        $route_uri = \Request::route()->uri();
        $arr_route_uri = explode ("/", $route_uri);
       
        if(count($arr_route_uri) > 2 && $arr_route_uri[1] == 'report'){ //we allow all users to access report for now - raven
            return $next($request);
        }

        if(count($arr_route_uri) >= 4 && $arr_route_uri[3] == 'refresh'){ //we allow all users to refresh transaction date for now - raven
            return $next($request);
        }
        
        if($user->userRole() == $role){
        
            // if($user->type==2){ //if user is sales type lets check for permissions
               
            //     if(session()->missing('role_pages')){
            //         $sales_permissions = RolePage::selectRaw("pages.url, pages.route_name, role_pages.page_function_access")
            //         ->join('pages', 'pages.id', 'role_pages.page_id')
            //         ->where('role_pages.type',2)->get()->toArray();
            //         $request->session()->put('role_pages', $sales_permissions);
            //     }

            //     $sales_permissions = collect($request->session()->get('role_pages')); 
            //     $is_allowed = $sales_permissions->where('route_name', $route_name);

            //     // $sales_permissions->where('price', 100);
            //     //dd($sales_permissions);
                
            //     if(count($is_allowed) > 0){
            //         return $next($request);
            //     }else{
            //         return redirect()->back()->withInput(); //we return to last page
            //     }
            // }

            return $next($request);
        }

        return $next($request);

        return response()->json(['You do not have permission to access for this page.']);
        //return response()->json(['role' => $role, 'type' => auth()->user()->type]);


    }

    // $route_name = \Request::route()->getName();
    // $route_uri = \Request::route()->uri();
    // $arr_route_uri = explode ("/", $route_uri);
    // $main_page_id = 0;
    // //this means that the uri we're accessing is a sub page or sub function of a page.
    // if(count($arr_route_uri) > 2){
    //     //lets find the main page of this uri and get the id
    //     $main_page = Page::where('url',$arr_route_uri[0]."/".$arr_route_uri[1])->first();
    //     if($main_page){
    //         $main_page_id = $main_page->id;
    //     }
    // }

    // $page = Page::firstOrCreate([
    //     'name' => $route_uri,
    //     'url' => $route_uri,
    //     'route_name' => $route_name,
    //     'main_page_id' => $main_page_id,
    // ]);

    // RolePage::firstOrCreate([
    //     'type' => 2,
    //     'page_function_access' => 1,
    //     'page_id' => $page->id
    // ]);

}
