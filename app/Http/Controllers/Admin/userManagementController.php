<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Vehicle;
use Validator;
use Str;
use App\Mail\NotificationEmail;
use Illuminate\Support\Facades\Mail;
use Swift_TransportException;
use Auth;

class userManagementController extends Controller
{   

     public function uploadFile(Request $request){
        $user = Auth::user();
        $request->validate([
            'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust the validation rules as needed.
        ]);

        $file = $request->file('file');
        $fileName = time() . '_' . $file->getClientOriginalName();
        $file->storeAs('uploads', $fileName, 'public');

        $user->update(['avatar' => $fileName]);

        return response()->json(['status' => true, 'message' => 'File uploaded successfully']);
    }

    public function index($type = 'dispatcher'){
        if (User::getType($type) !== false) {
            $vehicles = Vehicle::get();
            return view('admin.userManagement.index', compact('type', 'vehicles'));
        }else{
            return redirect()->back();
        }
    }


    public function store(Request $request){
        $vehicle_required = ($request->get('type') == 'driver')? 'required' : '';

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'birthdate' => 'required',
            'complete_address' => 'required',
            'contact_number' => 'required',
            'email_address' => 'required|email|unique:users,email_address,'. $request->get('id').',id,deleted_at,NULL',
            'driver_license_no' => $vehicle_required,
            'driver_license_expiration' => $vehicle_required,
            'color' => $vehicle_required,
            'vehicle_model' => $vehicle_required,
            'plate_number' => $vehicle_required,
            'vehicle_type' => $vehicle_required,
            'capacity' => $vehicle_required,
            'contact_person' => $vehicle_required,
            'contact_person_cp_number' => $vehicle_required,
        ]);
    	
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }else{
            
            $username = strtolower(explode(' ', $request->get('first_name'))[0].'_'.getMaxId());
            $password = strtolower(str_replace(' ', '_', $request->get('last_name')).'_'.(getMaxId()+rand(1,9)));
            $user = [];

            if (!empty($request->get('id'))) {
                $user = User::find($request->get('id'));
                $user->suffix = $request->get('suffix');
                $user->first_name = $request->get('first_name');
                $user->middle_name = $request->get('middle_name');
                $user->last_name = $request->get('last_name');
                $user->complete_address = $request->get('complete_address');
                $user->email_address = $request->get('email_address');
                $user->contact_number = $request->get('contact_number');
                $user->gender = $request->get('gender');
                $user->birthdate = $request->get('birthdate');
                $user->username = $request->get('username');

                if ($request->get('type') == 'driver') {
                    $user->driver_license_no = $request->get('driver_license_no');
                    $user->driver_license_expiration = $request->get('driver_license_expiration');
                    $user->conductor_name = $request->get('conductor_name');
                    $user->color = $request->get('color');
                    $user->vehicle_model = $request->get('vehicle_model');
                    $user->plate_number = $request->get('plate_number');
                    $user->vehicle_type = $request->get('vehicle_type');
                    $user->capacity = $request->get('capacity');
                    $user->contact_person = $request->get('contact_person');
                    $user->contact_person_cp_number = $request->get('contact_person_cp_number');
                }
                
                if ($user->save()) {
                    return response()->json(['status' => true, 'message' => ucfirst($request->get('type')).' saved successfully!', 'details' => false]);
                }
                
            }else{
                $user = new User;
                $user->suffix = $request->get('suffix');
                $user->first_name = $request->get('first_name');
                $user->middle_name = $request->get('middle_name');
                $user->last_name = $request->get('last_name');
                $user->complete_address = $request->get('complete_address');
                $user->email_address = $request->get('email_address');
                $user->contact_number = $request->get('contact_number');
                $user->gender = $request->get('gender');
                $user->birthdate = $request->get('birthdate');
                $user->username = $request->get('username');

                if ($request->get('type') == 'driver') {
                    $user->driver_license_no = $request->get('driver_license_no');
                    $user->driver_license_expiration = $request->get('driver_license_expiration');
                    $user->conductor_name = $request->get('conductor_name');
                    $user->color = $request->get('color');
                    $user->vehicle_model = $request->get('vehicle_model');
                    $user->plate_number = $request->get('plate_number');
                    $user->vehicle_type = $request->get('vehicle_type');
                    $user->capacity = $request->get('capacity');
                    $user->contact_person = $request->get('contact_person');
                    $user->contact_person_cp_number = $request->get('contact_person_cp_number');
                }
                
                $user->email_verified_at = $request->get('email_verified_at');
                $user->username = $username;
                $user->password = $password;
                $user->type = User::getType($request->get('type'));
                $user->status = 1;

                $details = ['username' => $username, 'password' => $password];

                try {
                    Mail::to($user->email_address)->send(new NotificationEmail($user, $password));
                } catch (Swift_TransportException $e) {
                   
                   
                }

                if ($user->save()) {
                    return response()->json(['status' => true, 'message' => ucfirst($request->get('type')).' saved successfully!', 'details' => $details]);
                }          
            }
        	
        }
    }

    public function list(Request $request, $type){
        // if (!empty($request->get('filter'))) {
        //     $User = User::whereNull('deleted_at')->where('type', 4)
        //     ->where('first_name','LIKE','%'.$request->get('filter').'%')
        //     ->orWhere('last_name','LIKE','%'.$request->get('filter').'%')
        //     ->where('type', 4)
        //     ->orWhere('address','LIKE','%'.$request->get('filter').'%')
        //     ->where('type', 4)
        //     ->orderBy('id', 'desc')
        //     ->get();
        // }else{
        //   $User = User::whereNull('deleted_at')->where('type', 4)->orderBy('id', 'desc')->get();
        // }

        $User = User::where('type', User::getType($type))->orderBy('id', 'desc')->get();
    	return response()->json(['status' => true, 'data' => $User]);
    }

    public function find($id){
		$user = User::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

    public function destroy($id){
        $user = User::where('id', $id)->update(['deleted_at' => now()]);
        return response()->json(['status' => true]);
    }
}
