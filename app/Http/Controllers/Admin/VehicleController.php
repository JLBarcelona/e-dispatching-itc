<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\User;
use Validator;

class VehicleController extends Controller
{
	public function index(){
		$vehicles = Vehicle::vehicleTypes();
		return view('admin.Vehicle.index', compact('vehicles'));
	}

	public function list(){
		$vehicles = User::where('type', 4)->get();
		return response()->json(['status' => true, 'data' => $vehicles ]);
	}

	public function save(Request $request, $id = ""){
		$validator = Validator::make($request->all(), [
			'color' => 'required',
			'vehicle_model' => 'required',
			'plate_number' => 'required',
			'type' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$vehicles = Vehicle::where('id', $id)->update([
					'color' => $request->get('color'),
					'vehicle_model' => $request->get('vehicle_model'),
					'plate_number' => $request->get('plate_number'),
					'type' => $request->get('type'),
				]);
				if($vehicles){
					return response()->json(['status' => true, 'message' => 'vehicles saved successfully!']);
				}
			}else{
				$vehicles = Vehicle::create([
					'color' => $request->get('color'),
					'vehicle_model' => $request->get('vehicle_model'),
					'plate_number' => $request->get('plate_number'),
					'type' => $request->get('type'),
				]);
				if($vehicles){
					return response()->json(['status' => true, 'message' => 'vehicles updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$vehicles = Vehicle::findOrFail($id);
		return response()->json(['status' => true, 'data' => $vehicles ]);
	}

	public function delete($id){
		$vehicles = Vehicle::findOrFail($id);
		if($vehicles->delete()){
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}