<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;


class profileSettingsController extends Controller
{
    public function index(){
    	$user = User::where('id', Auth::user()->id)->first();

    	return view('admin.profileSetting.index', compact('user'));
    }

    public function change_password(Request $request){

    	$validator = Validator::make($request->all(), [
            'password' => 'required',
			'new_password' => 'required',
			'confirm_password' => 'required',
			
            
        ]);
    	
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }else{
    		$user = User::where('id', Auth::user()->id)->first();

    		if (Hash::check($request->password, $user->password)){
    			
    			if ($request->new_password == $request->confirm_password) {
    			$user->password = $request->new_password;
    			$user->save();

    			return response()->json(['status' => true, 'message' => "Password Change!"]);
    			}else{
    				return response()->json(['status' => false, 'message' => "new password don't match!"]);
    			}
    		}else{
    			//old password dont matach
    			return response()->json(['status' => false, 'message' => "Old password incorrect!"]);
    		}

        }
     }

     public function store(Request $request){

     	$validator = Validator::make($request->all(), [
            'first_name' => 'required',
			'last_name' => 'required',
            'email_address' => 'required|email|unique:users,email_address,'. Auth::user()->id.',id,deleted_at,NULL',
            'username' => 'required|unique:users,username,'. Auth::user()->id.',id,deleted_at,NULL',
        ]);
    	
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }else{

        $user = User::where('id', Auth::user()->id)->first();
        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name = $request->last_name;
        $user->email_address = $request->email_address;
        $user->username = $request->username;
        $user->save();
        return response()->json(['status' => true, 'message' => 'Account Updated Successfully!']);
        	
        
     }
        }
}
