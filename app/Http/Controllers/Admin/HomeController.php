<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Models\Transaction;

use Auth;

class HomeController extends Controller
{
    public function index(){
    	
        return view('admin.home');
    }

    public function scanner(){
    	
        return view('admin.scan');
    }

    public function scanCode(Request $request){
    	$id = $request->get('id');
    	$tr = Transaction::where('id', $id)->whereDate('created_at', date('Y-m-d'));
    	if ($tr->count() > 0) {
    		$transaction = $tr->first();
    		switch ($transaction->status) {
	    		case '0':
	    			$transaction->departure_time = now();
	    			$transaction->status = 2;
	    			$transaction->save();
		    		return response()->json(['status' => true, 'message' => 'Route Transaction updated to departed. have a safe trip!']);
				break;

				case '1':
		    		return response()->json(['status' => false, 'message' => 'This QRcode has been already set to completed.']);
				break;
				
				case '2':
					$transaction->arrival_time = now();
	    			$transaction->status = 1;
	    			$transaction->save();
		    		return response()->json(['status' => true, 'message' => 'Route Transaction Dispatch completed.']);
				break;
				
				case '3':
		    		return response()->json(['status' => false, 'message' => 'Invalid QRCode!']);
				break;
	    	}
    	}else{
    		return response()->json(['status' => false, 'message' => 'Invalid or Expired QR Code!']);
    	}


    }
}



// const PENDING = 0;
// const ARRIVED = 1;
// const DEPARTED = 2;
// const CANCELLED = 3;
