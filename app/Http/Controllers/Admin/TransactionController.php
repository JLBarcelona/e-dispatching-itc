<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use Validator;
use Auth;
use PDF;

class TransactionController extends Controller
{
	public function index(){
		return view('admin.Transactions.index');
	}


	public function generatePDF(){
	    $data = [
            'logo' => asset('img/stgo.png'), // Path to your logo file
            'title' => 'REPUBLIC OF THE PHILIPPINES',
            'content' => 'INTEGRATED TERMINAL COMPLEX',
            'tdata' => Transaction::with(['driver','user'])->whereDate('created_at', date('Y-m-d'))->get(),
        ];
	    
	    $pdf = PDF::loadView('pdf.monitoring', $data);
	    $pdf->setPaper('legal', 'landscape');
	    return $pdf->stream('sample.pdf');
	}

	public function list(){
		$trans =  [];
		$user = Auth::user();
		if ($user->type == 4) {
			$trans = Transaction::with(['driver'])->where('driver_id', $user->id)->get();
		}else{
			$trans = Transaction::with(['driver'])->get();
		}
		return response()->json(['status' => true, 'data' => $trans ]);
	}

	public function save(Request $request, $id = ""){
		$validator = Validator::make($request->all(), [
			'route' => 'required',
			'driver' => 'required',
			'male' => 'required',
			'female' => 'required'
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$trans = Transaction::where('id', $id)->update([
					'driver_id' => $request->get('driver'),
					'male' => $request->get('male'),
					'female' => $request->get('female'),
					'routes' => $request->get('route'),
				]);
				if($trans){
					return response()->json(['status' => true, 'message' => 'vehicles saved successfully!']);
				}
			}else{
				$trans = Transaction::create([
					'user_id' => Auth::user()->id,
					'driver_id' => $request->get('driver'),
					'male' => $request->get('male'),
					'female' => $request->get('female'),
					'routes' => $request->get('route'),
					'status' => 0,
				]);
				if($trans){
					return response()->json(['status' => true, 'message' => 'vehicles updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$trans = Transaction::findOrFail($id);
		return response()->json(['status' => true, 'data' => $trans ]);
	}

	public function delete($id){
		$trans = Transaction::findOrFail($id);
		if($trans->delete()){
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}