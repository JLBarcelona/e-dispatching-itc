<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Artisan;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Str;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordEmail;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{   
 
    public function index(){
        return view('index');
    }

    public function authenticate(Request $request){
         $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            // return response()->json(['status' => false, 'error' => $validator->errors()]);
            return redirect()->route('login')->with('status', 'Please input your credentials.');
        }else{
            $credentials = $request->validate([
                'username' => ['required'],
                'password' => ['required'],
            ]);
     
            if (Auth::attempt($credentials,$request->remember)) {
                $request->session()->regenerate();
                $user = Auth::user();

                return redirect(RouteServiceProvider::HOME_ADMIN);
               

            }else{
                return redirect()->route('login')->with('status', 'Invalid Account!');
            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        Session::flush();
        Auth::guard('web')->logout();
        //Auth::logout();
        
        return redirect()->route('login');
    }

    public function forget_password(){
        return view('forgot_password');
    }

    public function forget_password_submit(Request $request){
        $rules = [
            'email_address' => 'required|email',
        ];

        $message = [];

        $validator = \Validator::make($request->all(),$rules,$message);

        if ($validator->fails()) {

            // return response()->json(['status' => false,'errors' => $validator->errors()]);
            return redirect()->route('auth.forget.password')->with('status', $validator->errors()->first('email_address'));

        }else{
           $user_check_email = User::where('email_address', $request->email_address);

           if ($user_check_email->count() > 0) {
            $user_account = $user_check_email->first();
            $user_account->forgot_password_token = Str::random(40);
            $user_account->save();

                 $data = [
                    'subject' => 'Reset Password',
                    'name' => $user_account->fullname,
                    'email_address' => $request->get('email_address'),
                    'url' => route('auth.change.password')."/".$user_account->forgot_password_token,
                ];

                $sentMail = Mail::to($request->get('email_address'))->send(new ForgotPasswordEmail($data, 'emails.request_token'));

                if ($sentMail){
                     return redirect()->route('auth.forget.password')->with('success', 'Reset Email URL has sent to your email, Please check your email!');
                }
           }else{
                 return redirect()->route('auth.forget.password')->with('status', 'Email Address is not registered.');
           }
        }
    }


    public function change_password($token){
        $has_user = User::where('forgot_password_token', $token);
        if ($has_user->count() > 0){
            return view('new_password')->with(compact('token'));
        }else{
            abort(403, 'Token Expired');
        }
    }

    // update password function
    public function update_pass(Request $request){
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        $token = $request->token;
        $check_password = 0;
        
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:7',
            'confirm_password' => 'required|min:7',
        ]);

        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }else{
            $users = User::where('forgot_password_token', $token)->first();
            $check_account = User::where('email_address', $users->email_address)->get();
    
            if ($password !== $confirm_password){
                return back()->with('message', 'Password do not match');
            }else{
                
            User::where('forgot_password_token', $token)->update([
                'password' => Hash::make($password),

            ]);
            User::where('email_address', $users->email_address)->update([
                'forgot_password_token' => null
            ]);
            return redirect()->route('login')->with('change_pass', 'Password updated successfully!');

            }
        }
    }
}
