<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    const VAN = 1;
    const BUS = 2;

    protected $table = 'vehicles';
    protected $fillable = [
        'color',
		'vehicle_model',
		'plate_number',
		'type',
    ];

    protected $appends = ['type_text'];


    public function getTypeTextAttribute(){
    	return $this->vehicleTypes()[$this->type];
    }

    public static function vehicleTypes(){
    	return [1 => 'van', 2 => 'bus'];
    }

}
