<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    const ADMIN = 1;

    const DISPATCHER = 2;

    const MONITOR = 3;

    const DRIVER = 4;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'complete_address',
        'email_address',
        'contact_number',
        'gender',
        'birthdate',
        'username',
        'vehicle_id',
        'email_verified_at',
        'type',
        'status',
        'forgot_password_token',
        'avatar',
        'contact_person'

    ];



    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d g:i A',
        'updated_at' => 'datetime:Y-m-d g:i A',
    ];

    protected $appends = ['has_password', 'fullname'];

    public function getFullnameAttribute(){
        $sfx = (!empty($this->suffix))? ' '.$this->suffix: '';
        $mname = (!empty($this->middle_name))? ' '.$this->middle_name[0]: '';
        return $this->last_name.', '.$this->first_name.$sfx.' '.$mname.'.';
    }

    public function getAvatarImg(){
        if (!empty($this->avatar)) {
            return asset('storage/uploads/'.$this->avatar);
        }else{
            asset('img/avat.jpg');
        }
    }

    public function getHasPasswordAttribute()
    {
        return ! empty($this->attributes['password']);
    }

    public static function getType($type){
        if (strtoupper($type) == 'DISPATCHER') {
            return User::DISPATCHER;
        }elseif (strtoupper($type) == 'MONITOR') {
            return User::MONITOR;
        }elseif (strtoupper($type) == 'DRIVER') {
            return User::DRIVER;
        }else{
            return false;
        }
    }

    public static function getTypeText($type){
        if ($type == 1) {
            return 'Admin';
        }elseif ($type == 2) {
            return 'Dispatcher';
        }elseif ($type == 3) {
            return 'Monitor';
        }elseif ($type == 4) {
            return 'Driver';
        }else{
            return false;
        }
    }
    
    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function userRole(&$id=NULL)
    {
        if (Auth::check()) 
        {   
            $user = User::find($this->id);
            if($user) return config('constants.user.role.'.$user->type);
            else Auth::logout();
        } 
        
        return null;
    }
    

}