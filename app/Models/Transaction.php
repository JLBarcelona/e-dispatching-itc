<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    const PENDING = 0;
    const ARRIVED = 1;
    const DEPARTED = 2;
    const CANCELLED = 3;

    protected $table = 'transactions';
    protected $fillable = [
        'qrslug',
        'user_id',
        'driver_id',
        'vehicle_id',
        'male',
        'female',
        'routes',
        'status',
        'departure_time',
        'arrival_time'
    ];

    protected $appends = ['status_text', 'status_color'];

    public function setQrslugAttribute($value){
        $this->attributes['qrslug'] = base64_encode($this->id.rand(100, 999));
    }

    public function getStatusTextAttribute(){
        if ($this->status == 0) {
            return 'PENDING';
        }elseif ($this->status == 1) {
            return 'DISPATCH COMPLETED';
        }elseif ($this->status == 2) {
            return 'DEPARTED';
        }elseif ($this->status == 3) {
            return 'CANCELLED';
        }
    }

    public function getStatusColorAttribute(){
        if ($this->status == 0) {
            return 'bg-secondary text-white';
        }elseif ($this->status == 1) {
            return 'bg-success text-white';
        }elseif ($this->status == 2) {
            return 'bg-info text-white';
        }elseif ($this->status == 3) {
            return 'bg-danger text-white';
        }
    }


    public function vehicle(){
        return $this->belongsTo(Vehicle::class, 'vehicle_id', 'id');
    }


    public function driver(){
        return $this->belongsTo(User::class, 'driver_id', 'id')->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id')->withTrashed();
    }

}
