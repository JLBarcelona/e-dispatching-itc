<?php 
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Transaction;


function getTodays(){
    return Transaction::with(['driver','user'])->whereDate('created_at', date('Y-m-d'))->get();
}

function getMaxId(){
    return User::max('id');
}

function getUsersCount($user_type){
    // ADMIN = 1;
    // DISPATCHER = 2;
    // MONITORING = 3;
    // DRIVER = 4;
	return User::where('type', $user_type)->count();
}

function getVehicleCount($user_type){
    // VAN = 1;
    // BUS = 2;
    // BUS = 3;

    switch ($user_type) {
        case '1':
                return User::where('type', 4)->where('vehicle_type', 'Van')->count();  
            break;
        case '2':
                return User::where('type', 4)->where('vehicle_type', 'Bus')->count();  
            break;
        case '3':
                return User::where('type', 4)->where('vehicle_type', 'Mini Bus')->count();  
            break;
    }


}


function getTransaction(){
    return Transaction::whereDate('created_at', date('Y-m-d'))->count();
}


function getTransactionMaleFemale($type){
    if ($type == 'male') {
        return Transaction::whereDate('created_at', date('Y-m-d'))->sum('male');
    }else{
        return Transaction::whereDate('created_at', date('Y-m-d'))->sum('female');
    }
}



function getUserByType($type){
	return User::where('type', $type)->get();
}


function getVehicles(){
	return Vehicle::get();
}


function getMyRoute(){
    $user = Auth::user();
    return $trans = Transaction::with(['vehicle', 'driver'])->where('driver_id', $user->id)->get();
}